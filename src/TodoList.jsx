import React, { Component } from "react";
import TodoItem from "./TodoItem"; 


class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem key={todo.id} handleDestroyClick={this.props.handleDestroyClick(todo.id)
              }
               title={todo.title} 
               completedLineThrough={todo.completed}
               handleToggleClick={this.props.handleToggleClick(todo)}/>
            ))}
          </ul>
        </section>
      );
    }
  }
  
  export default TodoList;